import requests
import json


SESSION = requests.Session()
SESSION.headers.update({
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
})
URL = 'http://rozklad.kpi.ua/Schedules/ViewSchedule.aspx?g=d40ad00d-893e-4cdf-b71d-d1b7460a00e6'


def save_schedule_html():
    res = SESSION.get(URL)
    with open('test.html', 'w', encoding='utf-8') as file:
        file.write(res.text)


def save_schedule_to_json(schedule, filename):
    with open(filename, 'w', encoding='utf-8') as file:
        file.write((json.dumps(schedule.to_dict(), sort_keys=False, indent=4, ensure_ascii=False)))


def get_schedule():
    res = SESSION.get(URL)
    return res.text
