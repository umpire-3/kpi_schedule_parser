from setuptools import setup, find_packages

requires = [
    'requests',
    'bs4'
]

setup(
    name='kpi_schedule_parser',
    version='0.0',
    description='Web parser for http://rozklad.kpi.ua',
    long_description='Web parser for http://rozklad.kpi.ua',
    classifiers=[
        'Programming Language :: Python'
    ],
    author='Vadym Biletskyi',
    author_email='umpire333@gmail.com',
    url='',
    keywords='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
)
