from .entity import Entity


class WeekDay:
    Monday = 1
    Tuesday = 2
    Wednesday = 3
    Thursday = 4
    Friday = 5
    Saturday = 6
    Sunday = 7

    __days_order__ = [
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    ]

    @staticmethod
    def to_week_day(day_number):
        return WeekDay.__days_order__[day_number]

    @staticmethod
    def week_days_iter():
        return iter(WeekDay.__days_order__)

    @staticmethod
    def get_working_week():
        return WeekDay.__days_order__[:-1]


class Week(Entity):
    def __init__(self):
        self._lessons = {}

    def add_lesson(self, week_day, order, lesson):
        if week_day not in self._lessons:
            self._lessons[week_day] = {}

        self._lessons[week_day][order] = lesson

    def to_dict(self):
        return {
            day: {
                order: lesson.to_dict()
                for order, lesson in lessons.items()
            }
            for day, lessons in self._lessons.items()
        }
