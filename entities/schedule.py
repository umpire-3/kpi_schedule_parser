from .entity import Entity


class Schedule(Entity):

    def __init__(self, schedule_dict):
        self._first_week = schedule_dict['First week']
        self._second_week = schedule_dict['Second week']

    def add_week_first_week(self, week):
        self._first_week = week

    def add_week_second_week(self, week):
        self._second_week = week

    def to_dict(self):
        return {
            'First week': self._first_week.to_dict(),
            'Second week': self._second_week.to_dict()
        }
