from .entity import Entity


class Lesson(Entity):
    def __init__(self, **kwargs):
        self._is_empty = kwargs.get('empty', False)

        if not self._is_empty:
            self._title = kwargs.get('title', 'No title')
            self._teacher = kwargs.get('teacher', 'No teacher')
            self._lecture_hall = kwargs.get('lecture_hall', 'Nowhere')

    def to_dict(self):
        return dict(
            title=self._title,
            teacher=self._teacher,
            lecture_hall=self._lecture_hall
        ) if not self._is_empty else 'No lesson'
