from .base_parser import BaseParser
from entities.week import Week, WeekDay


class WeekParser(BaseParser):

    def parse(self, soup):
        rows = soup.find_all('tr')[1:]

        week = Week()
        lesson_parser = self._get_parser_for('Lesson')

        for order, row in enumerate(rows):
            entries = row.find_all('td')[1:]
            for day, lesson in zip(WeekDay.get_working_week(), entries):
                week.add_lesson(day, order, lesson_parser.parse(lesson))

        return week
