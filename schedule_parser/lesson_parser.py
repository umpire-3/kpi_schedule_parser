from .base_parser import BaseParser
from entities.lesson import Lesson


class LessonParser(BaseParser):

    def parse(self, soup):
        details = soup.find_all('a')
        if details:
            title, teacher, lecture_hall = details
            return Lesson(
                title=title['title'],
                teacher=teacher['title'],
                lecture_hall=lecture_hall.text
            )

        return Lesson(empty=True)
