from .base_parser import BaseParser
from entities.schedule import Schedule


class ScheduleParser(BaseParser):

    def parse(self, soup):
        week_1 = soup.find('table', {'id': 'ctl00_MainContent_FirstScheduleTable'})
        week_2 = soup.find('table', {'id': 'ctl00_MainContent_SecondScheduleTable'})

        schedule = Schedule({
            'First week': self._get_parser_for('First week').parse(week_1),
            'Second week': self._get_parser_for('Second week').parse(week_2)
        })

        return schedule
