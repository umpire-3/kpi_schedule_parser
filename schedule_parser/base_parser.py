class BaseParser:

    def __init__(self, sub_parsers=None):
        self._sub_parsers = sub_parsers if sub_parsers else {}

    def _get_parser_for(self, name):
        return self._sub_parsers[name]

    def add_sub_parser(self, name, parser):
        self._sub_parsers[name] = parser

    def parse(self, soup):
        pass
