import sys
import loader
from bs4 import BeautifulSoup

from schedule_parser.schedule_parser import ScheduleParser
from schedule_parser.week_parser import WeekParser
from schedule_parser.lesson_parser import LessonParser


if __name__ == '__main__':
    html = loader.get_schedule()

    week_parser = WeekParser({
        'Lesson': LessonParser()
    })
    schedule_parser = ScheduleParser({
        'First week': week_parser,
        'Second week': week_parser
    })

    schedule = schedule_parser.parse(BeautifulSoup(html))
    print(schedule.to_dict())

    if len(sys.argv) > 1:
        file_name = sys.argv[1]
        loader.save_schedule_to_json(schedule, file_name)
